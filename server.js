var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : '54.149.101.118',
  user     : 'root',
  password : 'lahacks',
  database : 'parking_v2'
});

connection.connect()

var restify = require('restify');
var sanitizer = require('sanitizer');
//http://54.149.101.118/spots/park?lat=-118.448139&lng=34.0650348&bid=yeeee


function park(req, res, next) {
	//remove spots in 25ft radius
	console.log(req.query);
	if (req.query.bid === undefined || req.query.bid === null) {
	    	res.send(400);
    		next();
    		return;
  	}

  	if (req.query.lng === undefined || req.query.lng === null) {
    		res.send(400);
    		next();
    		return;
  	}

  	if (req.query.lat === undefined || req.query.lat === null) {
    		res.send(400);
    		next();
    		return;
  	}
	
	res.send(200);
	
	var bid_clean = sanitizer.sanitize(req.query.bid);
	var lng_clean = sanitizer.sanitize(req.query.lng);
	var lat_clean = sanitizer.sanitize(req.query.lat);
	
	connection.query("DELETE FROM open_spots WHERE ( 3959 * acos( cos( radians(" + mysql.escape(lat_clean) + ") ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(" + mysql.escape(lng_clean) + ") ) + sin( radians(" + mysql.escape(lat_clean) + ") ) * sin( radians( latitude ) ) ) )  < "+ mysql.escape("0.00473485") + ";");  //less than 25 feet away

	//write to parking table time of park
	connection.query("INSERT INTO parking_archive (latitude, longitude, bid) VALUES("+mysql.escape(lat_clean)+","+mysql.escape(lng_clean)+","+mysql.escape(bid_clean)+");");	
	//remove open spots from more than 15 minutes ago
	connection.query("DELETE FROM open_spots WHERE time_open < (NOW() - INTERVAL 20 MINUTE);");
	next();
}

//http://54.149.101.118/spots/leave?lat=-118.448139&lng=34.0650348&bid=yeeee
//
function leave(req, res, next) {
	console.log(req.query);
        if (req.query.bid === undefined || req.query.bid === null) {
                res.send(400);
                next();
                return;
        }

        if (req.query.lng === undefined || req.query.lng === null) {
                res.send(400);
                next();
                return;
        }

        if (req.query.lat === undefined || req.query.lat === null) {
                res.send(400);
                next();
                return;
        }

        res.send(200);
	var bid_clean = sanitizer.sanitize(req.query.bid);
        var lng_clean = sanitizer.sanitize(req.query.lng);
        var lat_clean = sanitizer.sanitize(req.query.lat);
	//Add spot to open list
	connection.query("INSERT INTO open_spots (latitude, longitude, bid) VALUES("+mysql.escape(lat_clean)+","+mysql.escape(lng_clean)+","+mysql.escape(bid_clean)+");");
	//fill out most recent leaving result in archive if null
	console.log("SELECT id FROM parking_archive WHERE bid="+mysql.escape(bid_clean)+" ORDER BY park_time DESC;");
	connection.query("SELECT id FROM parking_archive WHERE bid="+mysql.escape(bid_clean)+" ORDER BY park_time DESC;", function(err, rows, fields){
		if(rows.length > 0)
		{
			connection.query("UPDATE parking_archive SET leave_time=CURRENT_TIMESTAMP WHERE leave_time IS NULL AND id = " + rows[0].id + ";",
				function(err, rows, fields) { 
					connection.query("DELETE FROM parking_archive WHERE leave_time IS NULL AND bid = " + mysql.escape(bid_clean) + ";"); //Delete old null values (the phone died or someone else drove the car etc.
				}
			);
		}
	});	
	next();
}

///http://54.149.101.118/spots?lat=<lat>&lng=<lng>&radius=<miles (double) >
function getParkingSpots(req, res, next) {
  console.log(req.query);
  var lat_clean = sanitizer.sanitize(req.query.lat);
  var lng_clean = sanitizer.sanitize(req.query.lng);
  var rad_clean = sanitizer.sanitize(req.query.radius);
  connection.query("SELECT latitude AS lat, longitude as lng, UNIX_TIMESTAMP(time_open) as timestamp, ( 3959 * acos( cos( radians(" + mysql.escape(lat_clean) + ") ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(" + mysql.escape(lng_clean) + ") ) + sin( radians(" + mysql.escape(lat_clean) + ") ) * sin( radians( latitude ) ) ) ) AS distance FROM open_spots HAVING distance < "+ mysql.escape(rad_clean) + " ORDER BY distance;", function(err, rows, fields) {
    console.log(rows);
    res.send({"spots":rows});
  });

  next();
}


function getHeatMap(req, res, next) { 
  console.log(req.query);
  connection.query("SELECT latitude AS lat, longitude AS lng FROM parking_archive;", function(err, rows, fields) {
    res.send({"spots":rows});
  });
  next();
}

function getAverageDuration(req, res, next) {
  console.log("calling getAverage");
  connection.query("SELECT UNIX_TIMESTAMP(park_time) AS arrived, UNIX_TIMESTAMP(leave_time) AS departed FROM parking_archive;", function(err, rows, fields) {
    var totalTime = 0;
    var rowsCounted = 0;

    console.log("entering loop");
    for (var i = 0; i < rows.length; i++) {
      if (rows[i].departed != null) {
        totalTime += (rows[i].departed - rows[i].arrived);
        console.log(i + " departed: " + rows[i].departed);
        console.log(i + " arrived:  " + rows[i].arrived);
        rowsCounted++;
      }      
    }
    console.log("totalTime: " + totalTime);
    console.log("rowsCounted: " + rowsCounted);
    res.send({"duration":totalTime/rowsCounted});
  });
  next();
}
    
var server = restify.createServer();
server.use(restify.queryParser());
// If you ask me about it I'll explain it. -Alex
server.use(restify.jsonp());
server.get('/spots', getParkingSpots);
server.get('/spots/heatmap', getHeatMap);
server.get('/spots/park', park);
server.get('/spots/leave', leave);
server.get('/spots/duration', getAverageDuration);
server.listen(80, function() {
  console.log('%s listening at %s', server.name, server.url);
});
