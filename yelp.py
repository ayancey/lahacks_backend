# -*- coding: utf-8 -*-
# A lot has been taken from Yelp's API
# I'm going to cry 3 minutes left

import argparse
import json
import pprint
import sys
import urllib
import urllib2

import oauth2
import MySQLdb
import webbrowser

# MySQL stuff
db = MySQLdb.connect(host="54.149.101.118", user="root",  passwd="lahacks", db="parking_v2") # Maybe make this password not so obvious if we want to show it to people
cur = db.cursor()

API_HOST = 'api.yelp.com'


DEFAULT_TERM = raw_input()
DEFAULT_LOCATION = 'Pauley Pavilion, Los Angeles, CA'
SEARCH_LIMIT = 3
SEARCH_PATH = '/v2/search/'
BUSINESS_PATH = '/v2/business/'

# OAuth credential placeholders that must be filled in by users.
CONSUMER_KEY = '7oW_byVM9dQIV-evHvEVKg'
CONSUMER_SECRET = 'yRMEq6J6VlomaAm0GInTGKE3ujc'
TOKEN = 'bxRBayxDTz-QXK-cMBBeY4lxxfKqXkcG'
TOKEN_SECRET = 'BU2sd9wDi08712N8UgticcqUhkY'


def request(host, path, url_params=None):
    """Prepares OAuth authentication and sends the request to the API.

    Args:
        host (str): The domain host of the API.
        path (str): The path of the API after the domain.
        url_params (dict): An optional set of query parameters in the request.

    Returns:
        dict: The JSON response from the request.

    Raises:
        urllib2.HTTPError: An error occurs from the HTTP request.
    """
    url_params = url_params or {}
    url = 'http://{0}{1}?'.format(host, urllib.quote(path.encode('utf8')))

    consumer = oauth2.Consumer(CONSUMER_KEY, CONSUMER_SECRET)
    oauth_request = oauth2.Request(method="GET", url=url, parameters=url_params)

    oauth_request.update(
        {
            'oauth_nonce': oauth2.generate_nonce(),
            'oauth_timestamp': oauth2.generate_timestamp(),
            'oauth_token': TOKEN,
            'oauth_consumer_key': CONSUMER_KEY
        }
    )
    token = oauth2.Token(TOKEN, TOKEN_SECRET)
    oauth_request.sign_request(oauth2.SignatureMethod_HMAC_SHA1(), consumer, token)
    signed_url = oauth_request.to_url()
    
    print u'Querying {0} ...'.format(url)

    conn = urllib2.urlopen(signed_url, None)
    try:
        response = json.loads(conn.read())
    finally:
        conn.close()

    return response

def search(term, location):
    """Query the Search API by a search term and location.

    Args:
        term (str): The search term passed to the API.
        location (str): The search location passed to the API.

    Returns:
        dict: The JSON response from the request.
    """
    
    url_params = {
        'term': term.replace(' ', '+'),
        'location': location.replace(' ', '+'),
        'limit': SEARCH_LIMIT
    }
    return request(API_HOST, SEARCH_PATH, url_params=url_params)

def get_business(business_id):
    """Query the Business API by a business ID.

    Args:
        business_id (str): The ID of the business to query.

    Returns:
        dict: The JSON response from the request.
    """
    business_path = BUSINESS_PATH + business_id

    return request(API_HOST, business_path)

def query_api(term, location):
    """Queries the API by the input values from the user.

    Args:
        term (str): The search term to query.
        location (str): The location of the business to query.
    """
    response = search(term, location)

    businesses = response.get('businesses')

    if not businesses:
        print u'No businesses for {0} in {1} found.'.format(term, location)
        return

    business_id = businesses[0]['id']

    print u'{0} businesses found, querying business info for the top result "{1}" ...'.format(
        len(businesses),
        business_id
    )

    response = get_business(business_id)

    print u'Result for business "{0}" found:'.format(business_id)
    pprint.pprint(response, indent=2)

    biz_lat = response['location']['coordinate']['latitude']
    biz_long = response['location']['coordinate']['longitude']

    query = "SELECT latitude AS lat, longitude as lng, UNIX_TIMESTAMP(time_open) as timestamp, ( 3959 * acos( cos( radians(" + str(biz_lat) + ") ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(" + str(biz_long) + ") ) + sin( radians(" + str(biz_lat) + ") ) * sin( radians( latitude ) ) ) ) AS distance FROM open_spots ORDER BY distance;"
    print cur.execute(query)

    first = False
    thelat = ''
    thelong = ''
    for row in cur.fetchall() :
    	if first == False:
    		thelat = str(row[0])
    		thelong = str(row[1])
    		first = True
    	print str(row[0]) + ' ' + str(row[1] + ' ' + str(row[2]) + ' ' + str(row[3]))

    webbrowser.open("https://www.google.com/maps/dir/" + str(biz_lat) + ", " + str(biz_long) + "/" + str(thelat) + ', ' + str(thelong))

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('-q', '--term', dest='term', default=DEFAULT_TERM, type=str, help='Search term (default: %(default)s)')
    parser.add_argument('-l', '--location', dest='location', default=DEFAULT_LOCATION, type=str, help='Search location (default: %(default)s)')

    input_values = parser.parse_args()

    try:
        query_api(input_values.term, input_values.location)
    except urllib2.HTTPError as error:
        sys.exit('Encountered HTTP error {0}. Abort program.'.format(error.code))


if __name__ == '__main__':
    main()
