# Code to fuzz parking spots for LA Hacks 2014
import MySQLdb
import random

# MySQL stuff
db = MySQLdb.connect(host="54.149.101.118", user="root",  passwd="lahacks", db="parking_v2") # Maybe make this password not so obvious if we want to show it to people
cur = db.cursor()

# Number of fake spots to create
f_spots = 20
# Center Latitude
c_latitude = 34.070342
# Center Longitude
c_longitude = -118.446929
# Random Tolerance
r_tolerance = 0.01
# Delete old spots
delete_spots = True

# If true, deletes old spots...
if delete_spots:
	print cur.execute("DELETE FROM open_spots WHERE bid = 'From my iPhone';")

for x in range(f_spots):
	new_lat = c_latitude + random.uniform(r_tolerance * -1 , r_tolerance) 
	new_long = c_longitude + random.uniform(r_tolerance * -1, r_tolerance)
 	print cur.execute("INSERT INTO open_spots (latitude, longitude, time_open, bid) VALUES('" + str(new_lat) + "', '" + str(new_long) + "', FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()) - FLOOR(0 + (RAND() * 900))), 'From my iPhone');")

# Saves changes
db.commit()